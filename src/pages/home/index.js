/* eslint-disable no-new */

import SvgLineConnect from '@/libs/svg-line-connect'

import './home.css'

import LogoMain from './assets/logo.svg'
import MapBg from './assets/streaks.jpg'
import MapMain from './assets/map.jpg'

const logoMain = {
  render (vc) {
    return vc('svg', {

    }, [
      vc('g', {

      }, [
        vc('circle', {
          attrs: {
            cy: 0,
            cx: 0,
            r: '100%',
            fill: 'white'
          }
        }),

        vc(LogoMain)
      ])
    ])
  }
}

const Home = {
  components: {
    'logo-main': logoMain
  },
  mounted () {
    const svgLines = new SvgLineConnect({
      parent: this.$el.querySelector('.home'),
      elements: this.$el.querySelectorAll('.connect')
    })

    svgLines
      .order('attr')
      // .style({}) // Adds styles to the borders
      .draw()

    console.log(svgLines)
  },
  render (vc) {
    return vc('main', {

    }, [
      vc('section', {
        class: 'row middle-xs map',
        style: {
          'background-image': `url(${MapBg})`
        }
      }, [
        vc('div', {
          class: 'col-xs-offset-1 col-xs-10'
        }, [
          vc('div', {
            class: 'box interactive',
            style: {
              'background-image': `url(${MapMain})`
            }
          }, [
            vc('logo-main', {
              class: 'start-xs logo'
            })
          ])
        ])
      ]),

      vc('section', {
        class: 'row middle-xs nav'
      }, [
        vc('div', {
          class: 'col-xs-offset-1 col-xs-10'
        }, [
          this.$slots.nav
        ])
      ]),

      vc('section', {
        class: 'row middle-xs home content'
      }, [
        vc('div', {
          class: 'col-xs-offset-1 col-xs-10',
          style: {
            position: 'relative'
          }
        }, [
          vc('div', {
            class: 'row between-xs'
          }, [
            vc('div', {
              class: 'col-xs-12'
            }, [
              vc('div', {
                class: 'box card hero-text'
              }, [
                vc('h2', {}, [
                  'Dear Parents,'
                ]),

                vc('h3', {}, [
                  'We are here to present the best child transportation in all the land! Welcome and enjoy this experience'
                ])
              ])
            ])
          ]),

          vc('div', { class: 'row spacer' }),

          vc('div', {
            class: 'row between-xs'
          }, [
            vc('div', {
              class: 'col-xs-12 col-sm-4'
            }, [
              vc('div', {
                attrs: { 'data-order': 2 },
                class: 'box card connect'
              }, [
                vc('h2', {}, [
                  'How it Works'
                ])
              ])
            ]),
            vc('div', {
              class: 'col-xs-12 col-sm-7'
            }, [
              vc('div', {
                attrs: { 'data-order': 1 },
                class: 'box card connect'
              }, [
                vc('h2', {}, [ 'Sign up Now' ]),

                vc('h3', {}, [ 'or' ]),

                vc('h2', {}, [ 'Log In' ])
              ])
            ])
          ]),

          vc('div', { class: 'row spacer-large' }),

          vc('div', {
            class: 'row between-xs'
          }, [
            vc('div', {
              class: 'col-xs-12 col-sm-4'
            }, [
              vc('div', {
                attrs: { 'data-order': 3 },
                class: 'box card connect'
              }, [
                vc('h2', {}, [
                  'Schools'
                ]),

                vc('h2', {}, [
                  'We Cover'
                ])
              ])
            ]),

            vc('div', {
              class: 'col-xs-12 col-sm-3'
            }, [
              vc('div', {
                attrs: { 'data-order': 6 },
                class: 'box card tall connect'
              }, [
                vc('h2', {}, [
                  'SafeGuards'
                ])
              ])
            ]),

            vc('div', {
              class: 'col-xs-12 col-sm-3'
            }, [
              vc('div', {
                attrs: { 'data-order': 7 },
                class: 'box card tall connect'
              }, [
                vc('h2', {}, [
                  'Drivers'
                ])
              ])
            ])
          ]),

          vc('div', { class: 'row center-xs spacer' }),

          vc('div', {
            class: 'row'
          }, [
            vc('div', {
              class: 'col-xs-12 col-sm-4'
            }, [
              vc('div', {
                attrs: { 'data-order': 4 },
                class: 'connect',
                style: {
                  width: '2px',
                  height: '2px',
                  margin: 'auto'
                }
              })
            ]),

            vc('div', {
              class: 'col-xs-12 col-xs-offset-1 col-sm-3'
            }, [
              vc('div', {
                attrs: { 'data-order': 5 },
                class: 'connect',
                style: {
                  width: '2px',
                  height: '2px',
                  margin: 'auto'
                }
              })
            ])
          ])
        ])
      ])
    ])
  }
}

export default Home
export {

}
