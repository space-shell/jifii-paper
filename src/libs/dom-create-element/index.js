const ns = node => {
  switch (node) {
    case 'svg':
    case 'g':
    case 'text':
    case 'textpath':
    case 'rect':
    case 'circle':
    case 'point':
      return 'http://www.w3.org/2000/svg'
    case 'href':
      return 'http://www.w3.org/1999/xlink'
    default:
      return null
  }
}

const CE = (el, { // TODO Curry function to option parameters
  parent,
  style = {},
  ...attrs = {}
}, children = []) => {
  const element = (parent || document) // Attach new DOM element to document or provided parent
    .createElementNS(ns(el), el)

  Object.keys(attrs).map(attr => {
    element.setAttributeNS(ns(attr), attr, attrs[attr])
  })

  Object.keys(style).map(val => {
    element.style[style] = style[val]
  })

  children.map(child => {
    element.appendChild(child)
  })

  return element
}

export default CE
