/* global HTMLElement, HTMLCollection */
// svg, mount, setAttr, el
/* eslint-disable curly */

import { svg, mount, setStyle } from 'redom'

/**
 * @author James Nicholls
 *
 * Draws an SVG line between DOM Nodes
 *
 * @param  {[type]} elements [description]
 * @return {[type]}          [description]
 */

const SvgLineConnect = function ({
  parent = document.body,
  elements = HTMLCollection,
  margin = 0
}) {
  this.init = { parent, elements, margin }

  setStyle(this.parent, { position: 'relative' })

  this.canvas = svg('svg', {
    width: '100%',
    height: '100%',
    style: {
      position: 'absolute',
      top: 0,
      left: 0
    }
  })

  mount(this.parent, this.canvas)
}

// ACCESSORS //

/**
 * @accessor
 * Initial variables
 */
Object.defineProperty(SvgLineConnect.prototype, 'init', {
  get () {
    return {
      parent: this.parent,
      elements: this.elements,
      margin: this.margin
    }
  },
  set ({
    parent,
    elements,
    margin
  }) {
    this.parent = parent
    this.elements = [...elements]
    this.margin = margin
  }
})

// PROTO - Returns 'this'

// Sorts the node Array according to the order method
SvgLineConnect.prototype.update = function (method = 'auto', opts = {}) {
  this.draw()
  return ({
    auto: ({ x }) => {
      window.addEventListener('resize', () => {
        this.draw()
      })
    },
    delay: ({ delay = 250, throttled = false }) => {
      window.addEventListener('resize', () => {
        // only run if we're not throttled
        if (!throttled) {
          // actual callback action
          this.draw()
          // we're throttled!
          throttled = true
          // set a timeout to un-throttle
          setTimeout(() => {
            throttled = false
          }, delay)
        }
      })

      return this
    },
    debounce: ({ reverse = false }) => {
      return this
    }
  }[method])(opts)
}

// Sorts the node Array according to the order method
SvgLineConnect.prototype.order = function (input = 'next', opts = {}) {
  return ({
    attr: ({ attr = 'order' }) => {
      this.elements = this.elements.sort((a, b) =>
        a.dataset[attr] - b.dataset[attr]
      )

      return this
    },
    next: ({ reverse = false }) => {
      return this
    },
    nearestNode: ({ nodes = 1 }) => {
      return this
    },
    radius: ({ distance = 10 }) => {
      return this
    },
    // Sorts from 'direction' to oppisite e.g direction = left, to, right
    spiral: ({ direction = 'left' }) => {
      return this
    }
  }[input])(opts)
}

// PROTO - Returns a value

// Returns an array of lines that joins node elements together
SvgLineConnect.prototype.join = function (input = 'direct', opts = {}) {
  // const relations = SvgLineConnect.nodeDirections(this.elements)
  return ({
    direct: () => {
      // Calculates the path from the anchor points between two nodes
      // SvgLineConnect.anchorConnect()
      return this
    },
    rightAngle: () => {
      return this
    }
  }[input])(opts)
}

// Returns an array of path data
SvgLineConnect.prototype.export = function () {
  return true
}

// TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST
SvgLineConnect.prototype.draw = function () { // TODO Map elements to lines
  const line1 = SvgLineConnect.svgAnchorConnect({
    parent: this.parent,
    elem: this.elements[0],
    sibling: this.elements[1]
  })
  mount(this.canvas, line1)
  const line2 = SvgLineConnect.svgAnchorConnect({
    parent: this.parent,
    elem: this.elements[1],
    sibling: this.elements[2]
  })
  mount(this.canvas, line2)
  const line3 = SvgLineConnect.svgAnchorConnect({
    parent: this.parent,
    elem: this.elements[2],
    sibling: this.elements[3]
  })
  mount(this.canvas, line3)
  const line4 = SvgLineConnect.svgAnchorConnect({
    parent: this.parent,
    elem: this.elements[3],
    sibling: this.elements[4]
  })
  mount(this.canvas, line4)
  const line5 = SvgLineConnect.svgAnchorConnect({
    parent: this.parent,
    elem: this.elements[4],
    sibling: this.elements[5]
  })
  mount(this.canvas, line5)
  const line6 = SvgLineConnect.svgAnchorConnect({
    parent: this.parent,
    elem: this.elements[5],
    sibling: this.elements[6]
  })
  mount(this.canvas, line6)

  return true
}

// STATIC - Returns a value //

SvgLineConnect.svgPathDraw = function (path, parent = document.body) {
  return svg('path', {
    d: ``
  })
}

SvgLineConnect.svgLineDraw = function ({ a, b, parent = document.body }) {
  const { x: aX, y: aY } = a
  const { x: bX, y: bY } = b
  const { x: offsetX, y: offsetY } = parent.getBoundingClientRect()

  const style = {
    stroke: 'black',
    'stroke-width': '4px'
  }

  return svg('line', {
    x1: aX - offsetX,
    y1: aY - offsetY,
    x2: bX - offsetX,
    y2: bY - offsetY,
    ...style
  })
}

SvgLineConnect.svgAnchorConnect = function ({
  parent = HTMLElement,
  elem = HTMLElement,
  sibling = elem.nextElementSibling,
  margin = 0
}) {
  const { a, b } = SvgLineConnect.rectsEdgeAnchors({ elem, sibling })

  return SvgLineConnect.svgLineDraw({ a, b, parent })
}

// Connects the node center points
SvgLineConnect.svgNodeConnect = function ({
  elem = HTMLElement,
  sibling = elem.nextElementSibling
}) {
  return [
    ...Object.values(SvgLineConnect.centerPoint(elem)),
    ...Object.values(SvgLineConnect.centerPoint(sibling))
  ]
}

SvgLineConnect.rectsEdgeAnchors = function ({
  elem = HTMLElement,
  sibling = elem.nextElementSibling,
  margin = 0
}) {
  const ab = SvgLineConnect.elemsDirection({ elem, sibling })
  const ba = SvgLineConnect.elemsDirection({
    elem: sibling,
    sibling: elem
  })

  return {
    a: SvgLineConnect.rectEdgeAnchor({ elem, rads: ab }),
    b: SvgLineConnect.rectEdgeAnchor({ elem: sibling, rads: ba })
  }
}

SvgLineConnect.rectEdgeAnchor = function ({
  elem = HTMLElement,
  rads = 0,
  margin = 0
}) {
  const { width, height } = elem.getBoundingClientRect()
  const { PI } = Math
  const ratio = Math.atan(height / width)

  // Olie @ https://stackoverflow.com/questions/4061576/finding-points-on-a-rectangle-at-a-given-angle
  switch (true) { // TODO Refactor to DRY code
    case -ratio <= rads && rads < ratio: // QA
      return SvgLineConnect
        .edgeCenterPoint({
          elem,
          margin,
          edge: 'right'
        })
    case ratio <= rads && rads < PI - ratio: // QS
      return SvgLineConnect
        .edgeCenterPoint({
          elem,
          margin,
          edge: 'top'
        })
    case (PI - ratio <= rads && rads <= PI) || (-PI <= rads && rads < ratio - PI): // QT
      return SvgLineConnect
        .edgeCenterPoint({
          elem,
          margin,
          edge: 'left'
        })
    case ratio - PI <= rads && rads < -ratio: // QC
      return SvgLineConnect
        .edgeCenterPoint({
          elem,
          margin,
          edge: 'bottom'
        })
    default:
      throw new Error(`[SvgLineConnect.rectEdgeAnchor]: ${ratio} ${rads}`)
  }
}

SvgLineConnect.polyEdgeAnchor = function ({ elem = HTMLElement, rads = 0, margin = 0, sides = 6 }) {
  return true
}

SvgLineConnect.elemsDirection = function ({ elem = HTMLElement, sibling = elem.nextElementSibling }) {
  const { cx: eX, cy: eY } = SvgLineConnect.centerPoint(elem)
  const { cx: sX, cy: sY } = SvgLineConnect.centerPoint(sibling)
  return Math.atan2(eY - sY, sX - eX) // NOTE Y - coords flipped due to DOM axis
}

/**
 * [description]
 * @param  {HTLMElement} elem [description]
 * @return {[type]}      [description]
 */
SvgLineConnect.centerPoint = function (elem) {
  const {width, height, x, y} = elem.getBoundingClientRect()
  return {
    cx: x + (width / 2),
    cy: y + (height / 2)
  }
}

SvgLineConnect.edgePoints = function (elem = HTMLElement, margin = 0) {
  return ['top', 'left', 'right', 'bottom']
    .map(edge =>
      SvgLineConnect.edgeCenterPoint(edge, elem, margin)
    )
}

/**
 * [description]
 * @param  HTLMElement elem [description]
 * @return { x, y }     [description]
 */
SvgLineConnect.edgeCenterPoint = function ({ edge = 'top', elem = HTMLElement, margin = 0 }) {
  const { width, height, x, y } = elem.getBoundingClientRect()

  return ({
    top: () => ({
      x: x + (width / 2),
      y: y + margin
    }),
    right: () => ({
      x: x + width + margin,
      y: y + (height / 2)
    }),
    bottom: () => ({
      x: x + (width / 2),
      y: y + height - margin
    }),
    left: () => ({
      x: x - margin,
      y: y + (height / 2)
    })
  }[edge])()
}

export default SvgLineConnect
export {

}
