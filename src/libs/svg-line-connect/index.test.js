/* eslint-disable */

// jest.mock('./svg-line-connect.css', () => jest.fn())

import { JSDOM } from 'jsdom'
import { expect } from 'chai'

import Jest from 'jest'

import SvgLineConnect from './'

import MOCK from './mocks'

const dom = new JSDOM(MOCK)

const parent = dom.window.document.getElementById('SvgLineWraper')
const elements = 'box'

const test = new SvgLineConnect({ parent, elements })

describe('SVG Line Connect', () => {
  describe('Collects elements', () => {
    it('Retrieves child elements by class name', () => {
      expect(test._nodes).to.be.a('HTMLCollection')
      expect(test._nodes).to.have.all.keys('0', '1', '2', '3', '4')
      expect(test._nodes).to.not.have.property('5')
    })

    it('', () => {

    })
  })

  describe('Calculates Points', () => {
    it('', () => {

    })
  })

  describe('Calculates Lines', () => {
    it('', () => {

    })
  })

  describe('Draws Lines', () => {
    it('', () => {

    })
  })

  describe('Exports Lines', () => {
    it('', () => {

    })
  })
})
