/**
 * Genegates an SVG element filled with streaks that animate in and out
 * @param       {Number} [density=10] [description]
 * @param       {Number} [length=10]  [description]
 * @param       {[type]} [speed=1}]   [description]
 * @constructor
 */
const SvgStreakBg = function ({
  density = 10,
  length = 10,
  speed = 1
}) {
  this.density = density
  this.length = length
  this.speed = speed
}

export default SvgStreakBg
export {

}
