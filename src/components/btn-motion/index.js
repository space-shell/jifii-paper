import mojs from 'mo-js'

const BtnMotion = {
  mounted () {
    new mojs.Html({
      el: this.$el,
      width: '100px',
      height: '100px'
    })

    new mojs.Shape({
      parent: this.$el,
      shape: 'circle',
      scale: { 0 : 1 },
      duration: 1000,
      delay: 1000,
      easing: 'cubic.out',
      repeat: 999
    }).play()
  },
  render (vc) {
    return vc('div')
  }
}

export default BtnMotion
