/* eslint-disable no-new */

import SvgLineConnect from '@/libs/svg-line-connect'

import './vue-paper-cards.css'

const card = {
  functional: true,
  render (vc, { props }) {
    const { points } = props
    return vc('svg', {

    }, [
      vc('path', {
        attrs: {
          d: points
        }
      }, [])
    ])
  }
}

// const cardBackground = {
//   functional: true,
//   render () {
//
//   }
// }

const cardWide = {

}

const cardTall = {

}

const PaperCards = {
  data () {
    return {
      svg: undefined,
      loaded: false
    }
  },
  methods: {

  },
  computed: {
    paths () {
      return this.svg.paths
    }
  },
  mounted () {
    // this.svg = new SvgLineConnect({ parent: this.$el, ref: 'box' })
    //
    // this.loaded = true
  },
  render (vc) {
    return vc('div', {
      attrs: {
        id: 'paper-cards'
      }
    }, [

    ])
  }
}

export default PaperCards
export {
  card,
  cardWide,
  cardTall
}
