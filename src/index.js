/* eslint-disable no-new */

import Vue from 'vue'

import App from '@/app.js'

new Vue({
  el: '#root',
  components: {
    app: App
  },
  render (vc) {
    return vc('app')
  }
})
