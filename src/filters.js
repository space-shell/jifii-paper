const SvgFilters = {
  functional: true,
  render (vc, { props }) {
    return vc('svg', {
      attrs: {
        id: 'svg-filters'
      }
    }, [
      vc('defs', {}, [
        vc('filter', {
          attrs: {
            id: 'gauss-blur',
            x: '0',
            y: '0',
            width: '200%',
            height: '200%'
          }
        }, [
          vc('feOffset', {
            attrs: {
              result: 'offOut',
              in: 'SourceAlpha',
              dx: '6',
              dy: '6'
            }
          }, []),

          vc('feGaussianBlur', {
            attrs: {
              result: 'blurOut',
              in: 'offOut',
              stdDeviation: '6'
            }
          }, []),

          vc('feBlend', {
            attrs: {
              in: 'SourceGraphic',
              in2: 'blurOut',
              mode: 'normal'
            }
          }, [])
        ])
      ])
    ])
  }
}

export default SvgFilters
