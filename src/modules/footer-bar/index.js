import './footer.css'
import './reset.css'

import facebook from './assets/facebook.svg'
import instagram from './assets/instagram.svg'
import snapchat from './assets/snapchat.svg'
import whatsapp from './assets/whatsapp.svg'

// import './dev.css'

const Footer = {
  components: {
    facebook,
    instagram,
    snapchat,
    whatsapp
  },
  render (vc) {
    return vc('footer', {
      attrs: {
        id: 'foot'
      }
    }, [
      vc('div', {
        class: 'col-xs-offset-1 col-xs-10'
      }, [
        vc('div', {
          class: 'row'
        }, [
          vc('div', {
            class: 'col-xs-12'
          }, [
            vc('facebook', { // TODO MoJs Icons
              class: 'social-icon'
            }),

            vc('instagram', {
              class: 'social-icon'
            }),

            vc('snapchat', {
              class: 'social-icon'
            }),

            vc('whatsapp', {
              class: 'social-icon'
            })
          ])
        ]),

        vc('div', {
          class: 'row start-xs'
        }, [
          vc('div', {
            class: 'col-xs-12 col-sm-5'
          }, [
            vc('a', {}, [
              'Home'
            ])
          ]),

          vc('div', {
            class: 'col-xs-12 col-sm-4'
          }, [
            vc('a', {
              class: 'row'
            }, [
              'About'
            ]),

            vc('a', {
              class: 'row'
            }, [
              'About'
            ]),

            vc('a', {
              class: 'row'
            }, [
              'About'
            ]),

            vc('a', {
              class: 'row'
            }, [
              'About'
            ])
          ]),

          vc('div', {
            class: 'col-xs-12 col-sm-3'
          }, [
            vc('a', {}, [
              'F.A.Q\'s'
            ])
          ])
        ])
      ])
    ])
  }
}

export default Footer
export {

}
