/* eslint-disable no-new */

import mojs from 'mo-js'

import './navigation-bar.css'

const navBars = parent => { // TODO Complete SVC nav connectiong bars
  new mojs.Html({
    el: parent
  })

  class Heart extends mojs.CustomShape {
    getShape () { return '<path d="M92.5939814,7.35914503 C82.6692916,-2.45304834 66.6322927,-2.45304834 56.7076029,7.35914503 L52.3452392,11.6965095 C51.0327802,12.9714696 48.9328458,12.9839693 47.6203869,11.6715103 L47.6203869,11.6715103 L43.2705228,7.35914503 C33.3833318,-2.45304834 17.3213337,-2.45304834 7.43414268,7.35914503 C-2.47804756,17.1963376 -2.47804756,33.12084 7.43414268,42.9205337 L29.7959439,65.11984 C29.7959439,65.1323396 29.8084435,65.1323396 29.8084435,65.1448392 L43.2580232,78.4819224 C46.9704072,82.1818068 52.9952189,82.1818068 56.7076029,78.4819224 L70.1696822,65.1448392 C70.1696822,65.1448392 70.1696822,65.1323396 70.1821818,65.1323396 L92.5939814,42.9205337 C102.468673,33.12084 102.468673,17.1963376 92.5939814,7.35914503 L92.5939814,7.35914503 Z"></path>' }
    getLength () { return 200 }
  }
  mojs.addShape('heart', Heart)

  new mojs.Shape({
    shape: 'heart',
    fill: 'none',
    stroke: 'white',
    scale: { 0: 1 },
    strokeWidth: { 50: 0 },
    y: -20,
    duration: 1000
  })
}

const NavLink = {
  functional: true,
  render (vc, { children }) {
    return vc('div', {
      class: 'nav-link'
    }, [
      vc('h3', {}, [
        children[0]
      ])
    ])
  }
}

const NavigationBar = {
  data () {
    return {
      line: ''
    }
  },
  methods: {
    navPoints () {
      const { x: baseX, y: baseY } = this.$el.getBoundingClientRect()
      return this.$slots.default
        .reduce((str, point) => {
          const {
            x: pointX,
            y: pointY,
            width: pointW,
            height: pointH
          } = point.elm.getBoundingClientRect()

          return (str += `
            ${(pointX - baseX) + (pointW / 2)},
            ${(pointY - baseY) + (pointH / 2)}
          `)
        }, '')
    }
  },
  mounted () {
    this.line = this.navPoints()
  },
  render (vc) {
    return vc('nav', {
      class: 'box',
      style: {
        position: 'relative'
      },
      attrs: {
        id: 'navigation'
      }
    }, [
      vc('svg', {
        style: {
          position: 'absolute',
          'z-index': -1,
          'margin-top': '-8px'
        },
        attrs: {
          width: '100%',
          height: '100%'
        }
      }, [
        vc('polyline', {
          attrs: {
            points: (this.line || ''),
            'stroke': 'DimGrey',
            'stroke-width': '6'
          }
        })
      ]),

      vc('div', {
        ref: 'nav-container',
        class: 'row middle-xs between-xs'
      }, [
        this.$slots.default.map((slot, _, arr) => {
          return vc('div', {
            class: `col-xs-12 col-sm-${Math.round(12 / arr.length)}`
          }, [
            vc('div', {
              class: 'row center-xs'
            }, [slot])
          ])
        })
      ])
    ])
  }
}

export default NavigationBar
export {
  NavLink
}
