import HeaderBar from '@/modules/header-bar'
import NavigationBar, { NavLink } from '@/modules/navigation-bar'
import PageHome from '@/pages/home'
import FooterBar from '@/modules/footer-bar'
import BtnMotion from '@/components/btn-motion'

import SvgFilters from '@/filters.js'

import './typebase.css'
import './vars.css'
import './app.css'

const App = {
  components: {
    'svg-filters': SvgFilters,
    'header-bar': HeaderBar,
    'navigation-bar': NavigationBar,
    'nav-link': NavLink,
    'btn-motion': BtnMotion,
    'page-home': PageHome,
    'footer-bar': FooterBar
  },
  mounted () {
  },
  render (vc) {
    return vc('div', {
      attrs: {
        id: 'app'
      }
    }, [
      vc('svg-filters'),
      vc('header-bar'),
      vc('page-home', {}, [
        vc('navigation-bar', {
          slot: 'nav'
        }, [
          vc('nav-link', {

          }, [
            'Home'
          ]),

          vc('nav-link', {

          }, [
            'About'
          ]),

          vc('nav-link', {

          }, [
            'Contact'
          ]),

          vc('nav-link', {

          }, [
            'Search...'
          ])
        ])
      ]),
      vc('footer-bar')
    ])
  }
}

export default App
export {

}
